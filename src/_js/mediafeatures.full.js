function makeObject(){
var x;
var browser = navigator.appName;
if(browser == "Microsoft Internet Explorer"){
x = new ActiveXObject("Microsoft.XMLHTTP");
}else{
x = new XMLHttpRequest();
}
return x;
}

//global variables...
var page = makeObject();
var xmlobject= "";
var itemArray= new Array();
var intervalPause= -1;

function ajaxXML(){
	page.open('get', 'mediaplayer/armyfeatures/features.xml');
	page.onreadystatechange = parsePage;
	page.send('');
}

function parsePage(){
	if(page.readyState == 4){
		if(page.status == 200){
			xmlobject = page.responseXML;
			//alert(xmlobject);
			convertXML();
		}			
	}
}

function convertXML(){
	var root= xmlobject.getElementsByTagName('rss')[0];
	var channels = root.getElementsByTagName("channel");
	var items = channels[0].getElementsByTagName("item");
	var numitems= items.length;
	var navHTML= '<ul>';
	var thisClass='normal';
	var firstClass='normal';
	var mainDiv= document.getElementById('MFContainer');
	if(mainDiv.hasChildNodes()){
		for(var it=0; it<mainDiv.childNodes.length; it++){
			var childOb= mainDiv.childNodes.item(it);
			if(childOb.id === 'MFnav') var navOBJ= childOb;
		}
	}
	for(i=0;i<numitems;i++){
		itemArray[i]= new Array();
		if(items[i].getElementsByTagName('title')[0].hasChildNodes()){
			itemArray[i]['title']= items[i].getElementsByTagName('title')[0].firstChild.nodeValue;
		}else{
			itemArray[i]['title']= 'no title';
		}
		if(items[i].getElementsByTagName('description')[0].hasChildNodes()){
			itemArray[i]['desc']= items[i].getElementsByTagName('description')[0].firstChild.nodeValue;
			if(itemArray[i]['desc'].length > 240){
				var spacePlace= itemArray[i]['desc'].indexOf(' ',240);
				if(spacePlace != -1){
					itemArray[i]['desc'] = itemArray[i]['desc'].substr(0,spacePlace);
					itemArray[i]['desc'] += '...';
				}
			}
		}else{
			itemArray[i]['desc']= 'no description';
		}
		if(items[i].getElementsByTagName('guid')[0].hasChildNodes()){
			itemArray[i]['link']= items[i].getElementsByTagName('guid')[0].firstChild.nodeValue;
		}else{
			itemArray[i]['link']= 'no link';
		}
		if(items[i].getElementsByTagName('category')[0].hasChildNodes()){
			itemArray[i]['cat']= items[i].getElementsByTagName('category')[0].firstChild.nodeValue;
			if(itemArray[i]['cat'] === 'Article'){
				itemArray[i]['cat']= 'News';
			}
			thisClass= 'MF'+itemArray[i]['cat'];
			if(thisClass === 'MFLinks Package'){
				thisClass= 'MFNews';
			}
		}else{
			itemArray[i]['cat']= 'no category';
			thisClass= 'normal';
		}
		
		if(items[i].getElementsByTagName('category')[1] && items[i].getElementsByTagName('category')[1].hasChildNodes()){
			itemArray[i]['av']= items[i].getElementsByTagName('category')[1].firstChild.nodeValue;
			if(itemArray[i]['av'] === 'A/V'){
				itemArray[i]['av']= 'AV';
			}
			if(items[i].getElementsByTagName('category')[1].getAttribute('domain')){
				itemArray[i]['domain']= items[i].getElementsByTagName('category')[1].getAttribute('domain');
			}else{
				itemArray[i]['domain']= 'none';
			}
		}else{
			itemArray[i]['av']= 'none';
			itemArray[i]['domain']= 'none';
		}
		if(items[i].getElementsByTagName('enclosure')[0].getAttribute('url')){
			itemArray[i]['encl']= items[i].getElementsByTagName('enclosure')[0].getAttribute('url');
		}else{
			itemArray[i]['encl']= 'no enclosure';
		}
		if(i === 0){
			firstClass= thisClass;
			navHTML += '<li><a id="dot'+i+'" href="javascript:MFlaunch('+i+',\''+thisClass+'\',true)" class="'+thisClass+'">&bull;</a></li>';
		}else{
			navHTML += '<li><a id="dot'+i+'" href="javascript:MFlaunch('+i+',\''+thisClass+'\',true)" class="normal" onmouseover="this.className=\''+thisClass+'\'" onmouseout="this.className=\'normal\'">&bull;</a></li>';
		}
	}
	navHTML += '</ul>';
	MFlaunch(0,firstClass,false);		
	navOBJ.innerHTML= navHTML;
}
function getStyle(element, style){
	var value = element.style[style];
	if (!value) {
		if (document.defaultView && document.defaultView.getComputedStyle) {
			var css = document.defaultView.getComputedStyle(element, null);
			value = css ? css.getPropertyValue(style) : null;
		} else if (element.currentStyle) {
			value = element.currentStyle[style];
		}
	}
	
	if (window.opera && ['left', 'top', 'right', 'bottom'].include(style)){
		if (getStyle(element, 'position') == 'static') value = 'auto';
	}
	
	return value == 'auto' ? null : value;
}
function setStyle(element, style){
	for(k in style) element.style[k] = style[k];
}
function setOpacity(element, value){
	if (value == 1){
		setStyle(element, { opacity: 
			(/Gecko/.test(navigator.userAgent) && !/Konqueror|Safari|KHTML/.test(navigator.userAgent)) ? 
			0.999999 : null });
		if(/MSIE/.test(navigator.userAgent)) {
			setStyle(element, {filter: getStyle(element,'filter').replace(/alpha\([^\)]*\)/gi,'')});  
		}
	} else {
		if(value < 0.00001) value = 0;
		setStyle(element, {opacity: value});
		if(/MSIE/.test(navigator.userAgent)){
			setStyle(element, 
				{ filter: getStyle(element,'filter').replace(/alpha\([^\)]*\)/gi,'') +
				'alpha(opacity='+value*100+')' });
		}
	}
}
function getOpacity(element){
	var opacity;
	if (opacity = getStyle(element, 'opacity')){
		return parseFloat(opacity);
	}
	if (opacity = (getStyle(element, 'filter') || '').match(/alpha\(opacity=(.*)\)/)){
		if(opacity[1]){
			return parseFloat(opacity[1]) / 100;  
		}
	}
	return 1.0;
}
function fadeOldOut(i){
	var oldObj= document.getElementById('MFOldContainer');
	var newObj= document.getElementById('MFContainer');
	setOpacity(oldObj,i);
	i=i-0.05;
	if(typeof(v)!='undefined'){
		window.clearInterval(v);
	}
	if(i>0){
		v= window.setTimeout("fadeOldOut("+i+")",40);
	}else{
		oldObj.innerHTML=newObj.innerHTML;
		oldObj.className=newObj.className;
		setOpacity(oldObj,1);
	}
}
function resetInterval(intNum,nextClass){
	var oldObj= document.getElementById('MFOldContainer');
	
	if(typeof(t)!='undefined'){
		window.clearInterval(t);
	}
	t= window.setTimeout("MFlaunch("+intNum+",\'"+nextClass+"\',false)",15000);
}
function pausecomp(millis){
	var date = new Date();
	var curDate = null;
	do { curDate = new Date(); }while(curDate-date < millis);
}
function MFlaunch(elemNum,theClass,intPause){
	var extraLink= 'MORE';
	var newNav= '<ul>';
	var thisClass= 'normal';
	var nextClass= 'normal';
	var nextNum= elemNum+1;
	var linkname= 'dot'+elemNum;
	var mainDiv= document.getElementById('MFContainer');
	var oldObj= document.getElementById('MFOldContainer');
	oldObj.innerHTML= mainDiv.innerHTML;
	oldObj.className= mainDiv.className;
	/*setStyle(oldObj, {visibility: 'visible'});*/
	fadeOldOut(1);
	if(mainDiv.hasChildNodes()){
		for(var it=0; it<mainDiv.childNodes.length; it++){
			var childOb= mainDiv.childNodes.item(it);
			if(childOb.id === 'MFpause') var pauseOBJ= childOb;
			if(childOb.id === 'MFcat') var catOBJ= childOb;
			if(childOb.id === 'MFimage') var imageOBJ= childOb;
			if(childOb.id === 'MFdesc') var descOBJ= childOb;
			if(childOb.id === 'MFnav') var navOBJ= childOb;
		}
	}
	if(intervalPause === elemNum){
		intPause= false;
		intervalPause= -1;
		//pauseOBJ.style.visibility= 'hidden';
	}
	if(!intPause){
		if(nextNum === itemArray.length){
			nextNum= 0;
		}
		if(itemArray[nextNum]['cat'] === 'no category'){
			nextClass= 'normal';
		}else{
			nextClass= 'MF'+itemArray[nextNum]['cat'];
			if(nextClass === 'MFLinks Package'){
				nextClass= 'MFNews';
			}
		}
		resetInterval(nextNum,nextClass);
	}else{
		window.clearInterval(t);
		intervalPause= elemNum;
		//pauseOBJ.innerHTML= '<img src="images/buttons/pausebutton.gif" width="10">'; /*<a href="javascript:MFlaunch('+elemNum+',\''+theClass+'\',true)" title="unpause">||</a>*/
		//pauseOBJ.style.visibility= 'visible';
	}
	for(i=0;i<itemArray.length;i++){
		if(itemArray[i]['cat'] === 'no category'){
			thisClass= 'normal';
		}else{
			thisClass= 'MF'+itemArray[i]['cat'];
			if(thisClass === 'MFLinks Package'){
				thisClass= 'MFNews';
			}
		}
		if(i === elemNum){
			if(intPause){
				newNav += '<li><a id="dot'+i+'" href="javascript:MFlaunch('+i+',\''+thisClass+'\',true)"><img src="images/buttons/pausebutton.gif" width="10" border="0"></a></li>';
			}else{
				newNav += '<li><a id="dot'+i+'" href="javascript:MFlaunch('+i+',\''+thisClass+'\',true)" class="'+thisClass+'">&bull;</a></li>';	
			}
		}else{
			newNav += '<li><a id="dot'+i+'" href="javascript:MFlaunch('+i+',\''+thisClass+'\',true)" class="normal" onmouseover="this.className=\''+thisClass+'\'" onmouseout="this.className=\'normal\'">&bull;</a></li>';
		}	
	}
	newNav += '</ul>';
	navOBJ.innerHTML= newNav;
	if(itemArray[elemNum]['cat'] === 'News'){
		extraLink= 'VIEW STORY';
	}
	if(itemArray[elemNum]['cat'] === 'Image'){
		extraLink= 'VIEW IMAGE';
	}
	if(itemArray[elemNum]['cat'] === 'Slideshow'){
		extraLink= 'VIEW SLIDESHOW';
	}
	if(itemArray[elemNum]['cat'] === 'Video'){
		extraLink= 'PLAY VIDEO';
	}
	mainDiv.className= theClass+'B';
	catOBJ.className= theClass;
	catOBJ.innerHTML= itemArray[elemNum]['cat'].toUpperCase();
	imageOBJ.innerHTML= '<a href=\"'+itemArray[elemNum]["link"]+'\" target=\"_parent\"><img src=\"'+itemArray[elemNum]['encl']+'\"></a>';
	descString= '<p><a href=\"'+itemArray[elemNum]["link"]+'\" target=\"_parent\">'+itemArray[elemNum]["title"]+'</a></p><p>'+itemArray[elemNum]["desc"]+'<br/>&nbsp;</p><p><a href=\"'+itemArray[elemNum]["link"]+'\" target=\"_parent\">'+extraLink+'</a>&nbsp;&nbsp;';
	if(itemArray[elemNum]['av'] != 'none'){
		if(itemArray[elemNum]['av'] === 'Video' || itemArray[elemNum]['av'] === 'AV'){
			if(itemArray[elemNum]['domain'] === 'none'){
				descString+= '<a href=\"'+itemArray[elemNum]["link"]+'\"><img src=\"../../images/buttons/videoButton/medium-gif/video-icon45x14.gif\"></a>&nbsp;';
			}else{
				descString+= '<a href=\"javascript:launchPlayer('+itemArray[elemNum]["domain"]+')\"><img src=\"../../images/buttons/videoButton/medium-gif/video-icon45x14.gif\"></a>&nbsp;';
			}
		}
		if(itemArray[elemNum]['av'] === 'Audio' || itemArray[elemNum]['av'] === 'AV'){
			descString+= '<a href=\"'+itemArray[elemNum]["link"]+'\"><img src=\"../../images/buttons/videoButton/medium-gif/video-icon45x14.gif\"></a>';
		}
	}
	descOBJ.innerHTML= descString+'</p>';
}
